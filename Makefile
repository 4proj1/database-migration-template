deploy-dev:
	docker container prune
	docker volume prune
	docker-compose up

migration:
	docker exec toutpareilusermanager_usermanager_1 php /app/project/bin/console doctrine:migrations:migration
	docker exec toutpareilusermanager_usermanager_1 php /app/project/bin/console make:migration